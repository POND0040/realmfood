package com.pond.realmfood.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.pond.realmfood.R;
import com.pond.realmfood.database.DatabaseManager;
import com.pond.realmfood.dialog.AddFoodDialogFragment;
import com.pond.realmfood.dialog.FoodDetailDialogFragment;
import com.pond.realmfood.dialog.RemoveFoodDialogFragment;
import com.pond.realmfood.dialog.UpdateFoodDialogFragment;
import com.pond.realmfood.model.FoodModel;
import com.pond.realmfood.recyclerview.FoodAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainFragment extends Fragment implements
        FoodDetailDialogFragment.OnDialogListener,
        AddFoodDialogFragment.OnDialogListener,
        UpdateFoodDialogFragment.OnDialogListener,
        RemoveFoodDialogFragment.OnDialogListener {
    private static final String TAG = "MainFragment";

    private DatabaseManager databaseManager;
    private View mRootView;

    private List<FoodModel> mFoodModels;
    private FoodAdapter mFoodAdapter;

    private RecyclerView rcvFoods;
    private Button btnAddFood;

    public MainFragment() {
    }

    @Override
    public void onFoodDetailOkClick() {
        Log.e(TAG, "onFoodDetailOkClick: ");
    }

    @Override
    public void onAddFoodAddClick(FoodModel foodModel) {
        Log.e(TAG, "onAddFoodAddClick: " + foodModel);
        addFood(foodModel);
    }

    @Override
    public void onAddFoodCancelClick() {
        Log.e(TAG, "onAddFoodCancelClick: ");
    }

    @Override
    public void onUpdateFoodUpdateClick(FoodModel foodModel) {
        Log.e(TAG, "onUpdateFoodUpdateClick: " + foodModel);
        updateFood(foodModel);
    }

    @Override
    public void onUpdateFoodCancelClick() {
        Log.e(TAG, "onUpdateFoodCancelClick: ");
    }

    @Override
    public void onRemoveFoodRemoveClick(FoodModel foodModel) {
        Log.e(TAG, "onRemoveFoodRemoveClick: " + foodModel);
        removeFood(foodModel);
    }

    @Override
    public void onRemoveFoodCancelClick() {
        Log.e(TAG, "onRemoveFoodCancelClick: ");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        databaseManager = DatabaseManager.getInstance();
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchFoods();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    private void initInstances(View rootView, Bundle savedInstanceState) {
        mRootView = rootView;

        mFoodModels = new ArrayList<>();
        mFoodAdapter = new FoodAdapter(mFoodModels);

        mFoodAdapter.setOnItemClickListener(new FoodAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Log.e(TAG, "onItemClick: " + position);
                callFoodDetailDialog(mFoodModels.get(position));
            }

            @Override
            public void onUpdateClick(int position) {
                Log.e(TAG, "onUpdateClick: " + position);
                callUpdateFoodDialog(mFoodModels.get(position));
            }

            @Override
            public void onRemoveClick(int position) {
                Log.e(TAG, "onRemoveClick: " + position);
                callRemoveFoodDialog(mFoodModels.get(position));
            }
        });

        bindViews();
    }

    private void bindViews() {
        rcvFoods = mRootView.findViewById(R.id.rcv_foods);
        btnAddFood = mRootView.findViewById(R.id.btn_add_food);

        rcvFoods.setLayoutManager(new LinearLayoutManager(requireActivity()));
        rcvFoods.setHasFixedSize(true);
        rcvFoods.setAdapter(mFoodAdapter);

        btnAddFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callAddFoodDialog();
            }
        });
    }

    private void fetchFoods() {
        mFoodModels.clear();

        List<FoodModel> foodModels = databaseManager.getFoods();

        // mock data if empty
        if (foodModels.isEmpty()) {
            List<FoodModel> presetFoods = new ArrayList<>();
            presetFoods.add(new FoodModel("My Food", 10.0));
            presetFoods.add(new FoodModel("New Food", 10.0));

            for (FoodModel presetFood : presetFoods) {
                databaseManager.addFood(presetFood);
            }
        }
        // END mock data if empty

        mFoodModels.addAll(foodModels);
        mFoodAdapter.notifyDataSetChanged();
    }

    private void addFood(FoodModel foodModel) {
        databaseManager.addFood(foodModel);
        fetchFoods();
    }

    private void updateFood(FoodModel foodModel) {
        databaseManager.updateFood(foodModel);
        fetchFoods();
    }

    private void removeFood(FoodModel foodModel) {
        databaseManager.removeFood(foodModel.getId());
        fetchFoods();
    }

    private void callFoodDetailDialog(FoodModel foodModel) {
        FoodDetailDialogFragment fragment = new FoodDetailDialogFragment.Builder()
                .setFood(foodModel)
                .build();
        fragment.show(getChildFragmentManager(), "FoodDetailDialogFragment");
    }

    private void callAddFoodDialog() {
        AddFoodDialogFragment fragment = new AddFoodDialogFragment.Builder()
                .build();
        fragment.show(getChildFragmentManager(), "AddFoodDialogFragment");
    }

    private void callUpdateFoodDialog(FoodModel foodModel) {
        UpdateFoodDialogFragment fragment = new UpdateFoodDialogFragment.Builder()
                .setFood(foodModel)
                .build();
        fragment.show(getChildFragmentManager(), "UpdateFoodDialogFragment");
    }

    private void callRemoveFoodDialog(FoodModel foodModel) {
        RemoveFoodDialogFragment fragment = new RemoveFoodDialogFragment.Builder()
                .setFood(foodModel)
                .build();
        fragment.show(getChildFragmentManager(), "RemoveFoodDialogFragment");
    }

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
}