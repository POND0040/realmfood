package com.pond.realmfood.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.pond.realmfood.fragment.MainFragment;
import com.pond.realmfood.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.contentContainer, MainFragment.newInstance())
                    .commit();
        }
    }
}