package com.pond.realmfood.database;

import com.pond.realmfood.model.FoodModel;

import java.util.Objects;

import io.realm.Realm;
import io.realm.RealmResults;

public class DatabaseManager {
    private final Realm realm;
    private static DatabaseManager instance;

    private DatabaseManager() {
        realm = Realm.getDefaultInstance();
    }

    public static DatabaseManager getInstance() {
        if (instance == null) {
            instance = new DatabaseManager();
        }

        return instance;
    }

    public RealmResults<FoodModel> getFoods() {
        return realm.where(FoodModel.class).findAll();
    }

    public void addFood(FoodModel foodModel) {
        foodModel.setId(nextId());
        realm.beginTransaction();
        realm.insert(foodModel);
        realm.commitTransaction();
    }

    public int nextId() {
        Number foodModelMaxId = realm.where(FoodModel.class).max("id");
        if (foodModelMaxId == null)
            return 0;
        else
            return foodModelMaxId.intValue() + 1;
    }

    public void updateFood(FoodModel foodModel) {
        realm.beginTransaction();
        realm.insertOrUpdate(foodModel);
        realm.commitTransaction();
    }

    public void removeFood(int foodId) {
        realm.beginTransaction();
        Objects.requireNonNull(realm.where(FoodModel.class)
                .equalTo("id", foodId)
                .findFirst())
                .deleteFromRealm();
        realm.commitTransaction();
    }
}
