package com.pond.realmfood.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pond.realmfood.R;
import com.pond.realmfood.model.FoodModel;

import java.util.List;

public class FoodAdapter extends RecyclerView.Adapter<FoodViewHolder> {

    private List<FoodModel> mModels;
    private OnItemClickListener mOnItemClickListener;

    public FoodAdapter(List<FoodModel> mModels) {
        this.mModels = mModels;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onUpdateClick(int position);

        void onRemoveClick(int position);
    }


    @NonNull
    @Override
    public FoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_food, parent, false);
        FoodViewHolder foodViewHolder = new FoodViewHolder(view);
        return foodViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FoodViewHolder holder, final int position) {
        FoodModel model = mModels.get(position);

        holder.setTvFoodId(String.valueOf(model.getId()));
        holder.setTvFoodName(model.getName());
        holder.setTvFoodPrice(String.valueOf(model.getPrice()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    if (position != RecyclerView.NO_POSITION) {
                        mOnItemClickListener.onItemClick(position);
                    }
                }
            }
        });

        holder.btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    if (position != RecyclerView.NO_POSITION) {
                        mOnItemClickListener.onUpdateClick(position);
                    }
                }
            }
        });

        holder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    if (position != RecyclerView.NO_POSITION) {
                        mOnItemClickListener.onRemoveClick(position);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mModels.size();
    }
}
