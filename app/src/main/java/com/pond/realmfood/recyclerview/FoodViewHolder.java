package com.pond.realmfood.recyclerview;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pond.realmfood.R;

public class FoodViewHolder extends RecyclerView.ViewHolder {
    private TextView tvFoodId;
    private TextView tvFoodName;
    private TextView tvFoodPrice;
    public Button btnUpdate;
    public Button btnRemove;

    public FoodViewHolder(@NonNull View itemView) {
        super(itemView);
        tvFoodId = itemView.findViewById(R.id.tv_food_id);
        tvFoodName = itemView.findViewById(R.id.tv_food_name);
        tvFoodPrice = itemView.findViewById(R.id.tv_food_price);
        btnUpdate = itemView.findViewById(R.id.btn_update);
        btnRemove = itemView.findViewById(R.id.btn_remove);
    }

    public void setTvFoodId(String foodId) {
        tvFoodId.setText(foodId);
    }

    public void setTvFoodName(String foodName) {
        tvFoodName.setText(foodName);
    }

    public void setTvFoodPrice(String foodPrice) {
        tvFoodPrice.setText(foodPrice);
    }
}
