package com.pond.realmfood.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.pond.realmfood.R;
import com.pond.realmfood.model.FoodModel;


public class RemoveFoodDialogFragment extends DialogFragment {
    private static final String KEY_FOOD = "key_food";

    private TextView tvFoodId;
    private TextView tvFoodName;
    private TextView tvFoodPrice;
    private Button btnRemove;
    private Button btnCancel;

    private FoodModel foodModel;

    public interface OnDialogListener {
        void onRemoveFoodRemoveClick(FoodModel foodModel);

        void onRemoveFoodCancelClick();
    }

    public static RemoveFoodDialogFragment newInstance(FoodModel foodModel) {
        RemoveFoodDialogFragment fragment = new RemoveFoodDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_FOOD, foodModel);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            restoreArguments(getArguments());
        } else {
            restoreInstanceState(savedInstanceState);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_remove_food, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindView(view);
        setupView();
    }

    private void bindView(View view) {
        tvFoodId = view.findViewById(R.id.tv_food_id);
        tvFoodName = view.findViewById(R.id.tv_food_name);
        tvFoodPrice = view.findViewById(R.id.tv_food_price);
        btnRemove = view.findViewById(R.id.btn_remove);
        btnCancel = view.findViewById(R.id.btn_cancel);
    }

    private void setupView() {
        tvFoodId.setText(String.valueOf(foodModel.getId()));
        tvFoodName.setText(foodModel.getName());
        tvFoodPrice.setText(String.valueOf(foodModel.getPrice()));

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnDialogListener listener = RemoveFoodDialogFragment.this.getOnDialogListener();
                if (listener != null) {
                    listener.onRemoveFoodRemoveClick(foodModel);
                }
                RemoveFoodDialogFragment.this.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnDialogListener listener = RemoveFoodDialogFragment.this.getOnDialogListener();
                if (listener != null) {
                    listener.onRemoveFoodCancelClick();
                }
                RemoveFoodDialogFragment.this.dismiss();
            }
        });
    }

    private OnDialogListener getOnDialogListener() {
        Fragment fragment = getParentFragment();
        try {
            if (fragment != null) {
                return (OnDialogListener) fragment;
            } else {
                return (OnDialogListener) getActivity();
            }
        } catch (ClassCastException ignored) {
        }
        return null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_FOOD, foodModel);
    }

    private void restoreInstanceState(Bundle bundle) {
        foodModel = bundle.getParcelable(KEY_FOOD);
    }

    private void restoreArguments(Bundle bundle) {
        foodModel = bundle.getParcelable(KEY_FOOD);
    }

    public static class Builder {
        private FoodModel foodModel;

        public Builder() {
        }

        public Builder setFood(FoodModel food) {
            this.foodModel = food;
            return this;
        }

        public RemoveFoodDialogFragment build() {
            return RemoveFoodDialogFragment.newInstance(foodModel);
        }
    }
}
