package com.pond.realmfood.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.pond.realmfood.R;
import com.pond.realmfood.model.FoodModel;


public class AddFoodDialogFragment extends DialogFragment {
    private EditText edtFoodName;
    private EditText edtFoodPrice;
    private Button btnAdd;
    private Button btnCancel;

    public interface OnDialogListener {
        void onAddFoodAddClick(FoodModel foodModel);

        void onAddFoodCancelClick();
    }

    public static AddFoodDialogFragment newInstance() {
        AddFoodDialogFragment fragment = new AddFoodDialogFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            restoreArguments(getArguments());
        } else {
            restoreInstanceState(savedInstanceState);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_add_food, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindView(view);
        setupView();
    }

    private void bindView(View view) {
        edtFoodName = view.findViewById(R.id.edt_food_name);
        edtFoodPrice = view.findViewById(R.id.edt_food_price);
        btnAdd = view.findViewById(R.id.btn_add);
        btnCancel = view.findViewById(R.id.btn_cancel);
    }

    private void setupView() {
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnDialogListener listener = AddFoodDialogFragment.this.getOnDialogListener();
                if (listener != null) {
                    String foodName = edtFoodName.getText().toString();
                    String foodPrice = edtFoodPrice.getText().toString();
                    FoodModel foodModel = new FoodModel(foodName, Double.parseDouble(foodPrice));
                    listener.onAddFoodAddClick(foodModel);
                }
                AddFoodDialogFragment.this.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnDialogListener listener = AddFoodDialogFragment.this.getOnDialogListener();
                if (listener != null) {
                    listener.onAddFoodCancelClick();
                }
                AddFoodDialogFragment.this.dismiss();
            }
        });
    }

    private OnDialogListener getOnDialogListener() {
        Fragment fragment = getParentFragment();
        try {
            if (fragment != null) {
                return (OnDialogListener) fragment;
            } else {
                return (OnDialogListener) getActivity();
            }
        } catch (ClassCastException ignored) {
        }
        return null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void restoreInstanceState(Bundle bundle) {
    }

    private void restoreArguments(Bundle bundle) {
    }

    public static class Builder {
        public Builder() {
        }

        public AddFoodDialogFragment build() {
            return AddFoodDialogFragment.newInstance();
        }
    }
}
