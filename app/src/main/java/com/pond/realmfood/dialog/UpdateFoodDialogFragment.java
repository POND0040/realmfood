package com.pond.realmfood.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.pond.realmfood.R;
import com.pond.realmfood.model.FoodModel;


public class UpdateFoodDialogFragment extends DialogFragment {
    private static final String KEY_FOOD = "key_food";

    private TextView tvFoodId;
    private EditText edtFoodName;
    private EditText edtFoodPrice;
    private Button btnUpdate;
    private Button btnCancel;

    private FoodModel foodModel;

    public interface OnDialogListener {
        void onUpdateFoodUpdateClick(FoodModel foodModel);

        void onUpdateFoodCancelClick();
    }

    public static UpdateFoodDialogFragment newInstance(FoodModel foodModel) {
        UpdateFoodDialogFragment fragment = new UpdateFoodDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_FOOD, foodModel);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            restoreArguments(getArguments());
        } else {
            restoreInstanceState(savedInstanceState);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_update_food, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindView(view);
        setupView();
    }

    private void bindView(View view) {
        tvFoodId = view.findViewById(R.id.tv_food_id);
        edtFoodName = view.findViewById(R.id.edt_food_name);
        edtFoodPrice = view.findViewById(R.id.edt_food_price);
        btnUpdate = view.findViewById(R.id.btn_update);
        btnCancel = view.findViewById(R.id.btn_cancel);
    }

    private void setupView() {
        tvFoodId.setText(String.valueOf(foodModel.getId()));
        edtFoodName.setText(foodModel.getName());
        edtFoodPrice.setText(String.valueOf(foodModel.getPrice()));

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnDialogListener listener = UpdateFoodDialogFragment.this.getOnDialogListener();
                if (listener != null) {
                    String foodName = edtFoodName.getText().toString();
                    String foodPrice = edtFoodPrice.getText().toString();
                    FoodModel updateFoodModel = new FoodModel(foodName, Double.parseDouble(foodPrice));
                    updateFoodModel.setId(foodModel.getId());
                    listener.onUpdateFoodUpdateClick(updateFoodModel);
                }
                UpdateFoodDialogFragment.this.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnDialogListener listener = UpdateFoodDialogFragment.this.getOnDialogListener();
                if (listener != null) {
                    listener.onUpdateFoodCancelClick();
                }
                UpdateFoodDialogFragment.this.dismiss();
            }
        });
    }

    private OnDialogListener getOnDialogListener() {
        Fragment fragment = getParentFragment();
        try {
            if (fragment != null) {
                return (OnDialogListener) fragment;
            } else {
                return (OnDialogListener) getActivity();
            }
        } catch (ClassCastException ignored) {
        }
        return null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_FOOD, foodModel);
    }

    private void restoreInstanceState(Bundle bundle) {
        foodModel = bundle.getParcelable(KEY_FOOD);
    }

    private void restoreArguments(Bundle bundle) {
        foodModel = bundle.getParcelable(KEY_FOOD);
    }

    public static class Builder {
        private FoodModel foodModel;

        public Builder() {
        }

        public Builder setFood(FoodModel food) {
            this.foodModel = food;
            return this;
        }

        public UpdateFoodDialogFragment build() {
            return UpdateFoodDialogFragment.newInstance(foodModel);
        }
    }
}
